# README #

This repo houses a pet project of mine designed to make Half-Life 2 a bit HD-er. It's aim is to preserve the gameplay, environment, and mood of the vanilla game. It just makes things a little crisper. :)

### How do I get set up? ###

To install this pack, just download the repo as a ZIP using the sidebar, then create a folder in {STEAM FOLDER}/SteamApps/common/Half-Life 2/hl2 called "custom" and move it into there. Next time you start up Half-Life 2, you should see the new textures.

### Contribution guidelines ###

Have a texture you want added I haven't gotten around to yet? Fork the repo and add it, and I may merge it! Have a texture you think is better than one of mine? You're probably wrong because mine are the best, but in case you're not, do the same thing and I may replace it with yours!

### Legal ###

I must stress that I do not own all textures in this repo. The original images are all available to the public on the Internet. However, I have made modifications to some and compiled them into a Source addon. That's what makes the repo special. :)

Given the nature of the content, this pack is presented under a GPLv3 license. You may modify and redistribute it as you wish so long as your derivative is released under the same license.